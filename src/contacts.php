<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require 'invalidInputException.php';

class contacts
{
    public $pdo;

    public function init($databaseName)
    {
        try {
            if ($databaseName !== '') {
                $this->pdo = new PDO('sqlite:'.__DIR__.'/'.$databaseName);
            // echo dirname(__FILE__).'/'.$databaseName;
            } else {
                $this->pdo = new PDO('sqlite:'.__DIR__.'/contacts.sqlite');
            }

            $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return true;
        } catch (Exception $e) {
            echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();

            return false;
        }
    }

    // permet de de renvoyer les détails d'un contact
    public function getContact($id)
    {
        if (empty($id)) {
            throw new invalidInputException("l'id doit être renseignés");
        }
        if (!is_numeric($id) || $id < 0) {
            throw new invalidInputException("l'id doit être un entier non nul");
        }
        $req = $this->pdo->query('SELECT * from contacts where id ='.$id);

        $row = $req->fetchAll();

        // si req ok (!false)
        if ($req) {
            // on renvoie le 1er et seul élément du tableau de résultats
            return $row[0];
        }
    }

    public function searchContact($search)
    {
        if (empty($search)) {
            throw new invalidInputException('search doit être renseigné');
        }
        if (!is_string($search)) {
            throw new invalidInputException('search doit être renseigné');
        }
        $req = "SELECT * from contacts where nom like '%".$search."%' or prenom like '%".$search."%'";

        $res = $this->pdo->query($req);

        $row = $res->fetchAll();

        // si req ok (!false)
        if ($res) {
            return $row;
        }
    }

    public function getAllContacts()
    {
        $req = $this->pdo->query('SELECT * from contacts');

        $row = $req->fetchAll();

        // si req ok (!false)
        if ($req) {
            return $row;
        }
    }

    public function createContact($nom, $prenom)
    {
        if (empty($nom) || empty($prenom)) {
            // echo "nom ou prenom vide creationContact";
            throw new invalidInputException('le nom et le prenom doivent être renseignés');
        }
        if (!is_string($nom) || !is_string($prenom)) {
            // echo "nom ou prenom vide creationContact";
            throw new invalidInputException('le nom et le prenom doivent être renseignés');
        }
        $stmt = $this->pdo->prepare('INSERT INTO contacts (nom, prenom) VALUES (:nom, :prenom)');

        return $stmt->execute([
            'nom' => $nom,
            'prenom' => $prenom,
        ]);
    }

    public function updateContact($id, $nom, $prenom)
    {
        if (empty($id) || empty($nom) || empty($prenom)) {
            throw new invalidInputException("l'id, le nom et le prenom doivent être renseignés");
        }
        if (!is_string($nom) || !is_string($prenom)) {
            // echo "nom ou prenom vide creationContact";
            throw new invalidInputException('le nom et le prenom doivent être renseignés');
        }
        if (!is_numeric($id) || $id < 0) {
            throw new invalidInputException("l'id doit être un entier non nul");
        }
        $stmt = $this->pdo->prepare('UPDATE contacts SET nom=:nom, prenom=:prenom where id=:id');

        return $stmt->execute([
            'nom' => $nom,
            'prenom' => $prenom,
            'id' => $id,
        ]);
    }

    public function deleteContact($id)
    {
        if (null === $id) {
            throw new invalidInputException("l'id doit être renseigné");
        }
        if (!is_numeric($id) || $id < 0) {
            throw new invalidInputException("l'id doit être un entier non nul");
        }
        $stmt = $this->pdo->prepare('DELETE from contacts where id=:id');

        return $stmt->execute([
            'id' => $id,
        ]);
    }

    public function deleteAllContact()
    {
        return $this->pdo->query('DELETE from contacts');
    }
}
